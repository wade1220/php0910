<?php
require __DIR__. '/__connect_db.php';

$result = [                                                 //定義一個陣列，讓使用者看訊息(知道資料使否送出成功)
    'success' => false,
    'code' => 400,
    'info' => '資料欄位不足',
    'post' => $_POST,                  //直接看到我們送出的是甚麼東西
];


# 如果沒有輸入必要欄位
if(empty($_POST['email']) or empty($_POST['password'])){
    echo json_encode($result, JSON_UNESCAPED_UNICODE);      //json_encode()轉換成json的格式，json_decode()轉換成原來的格式
    exit;                                                   //JSON_UNESCAPED_UNICODE，不要做中文字的跳脫
}

#快速修改多個東西可用政正則， ctrl d 叫出修改 然後打上 \[value\-\d\]

$sql = "SELECT `id`, `email`, `nickname` FROM `members` WHERE `email`=? AND `password`=SHA1(?)";        //SHA1(?)解密，SELECT的和後面的WHERE沒有關係
            

$stmt = $pdo->prepare($sql);    #prepare() 可防止有人很熟SQL語法，直接在欄位輸入SQL語法，操作我的資料庫(SQL injection);

$stmt->execute([                #所有欄位順序都要對應
    $_POST['email'],
    $_POST['password'],
]);

//另一種寫法
// $sql = sprintf("INSERT INTO `address_book`(
//     `name`, `email`, `mobile`, `birthday`, `address`, `created_at`
//     ) VALUES (%s, %s, %s, %s, %s, NOW())"
//     ,$pdo->quote($_POST['name']),               //quote()是一個會自動幫你加上''的function，如果值裡面有'，則會自動幫你做跳脫
//     $pdo->quote($_POST['email']),
//     $pdo->quote($_POST['mobile']),
//     $pdo->quote($_POST['birthday']),
//     $pdo->quote($_POST['address'])              //最後一個不能有逗點
// );   

// echo $sql;
// $stmt = $pdo->query($sql);


$row = $stmt->fetch();
if($stmt->rowCount()==1){ 
    $_SESSION['loginUser'] = $row;
    
    $result['success'] = true;
    $result['code'] = 200;
    $result['info'] = '登入成功';
} else {
    $result['code'] = 420;
    $result['info'] = '登入失敗';
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);



 //echo $stmt->rowCount();     #可以計算成功筆數，本範例為一筆

 









