<?php
require __DIR__.'/0823_connect_db.php';
$page_name = 'data_list';
$page_title = '資料列表';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

$per_page = 10;

$t_sql = "SELECT COUNT(`sid`) FROM `address_book` ";
$t_stmt = $pdo->query($t_sql);
$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0];
$totalPages = ceil($totalRows/$per_page);

if($page < 1){
    header('Location: 0823_data_list.php');
    exit;
}
if($page > $totalPages) {
    header('Location: 0823_data_list.php?page='.$totalPages);
    exit;
}

$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s",     //LIMIT 填兩個值代表要顯示幾頁到幾頁，第一個值是索引值(從哪開始)，第二個值是顯示幾筆
        ($page-1)*$per_page,
        $per_page
);
$stmt = $pdo->query($sql);
// $stmt = $pdo->query("SELECT * FROM `address_book` ORDER BY `sid` DESC");

?>
<?php include __DIR__.'/0823_html_head.php' ?>
<?php include __DIR__.'/0823_navbar.php' ?>
<div class="container">
    <div style="margin-top: 2rem">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $page-1 ?>">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                </li>

                <?php
                $p_start = $page-2;
                $p_end = $page+2;

                for($i=$p_start; $i<=$p_end; $i++) {
                    if($i<1 or $i>$totalPages) continue;
                ?>
                <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                    <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a>
                </li>
                <?php } ?>

                <li class="page-item">
                    <a class="page-link" href="?page=<?= $page+1 ?>">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </nav>



        <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">姓名</th>
                <th scope="col">電子郵箱</th>
                <th scope="col">手機</th>
                <th scope="col">生日</th>
                <th scope="col">地址</th>
            </tr>
        </thead>
        <tbody>
        <?php while($r=$stmt->fetch()) { ?>
            <tr>
            <td><?= htmlentities($r['sid']) ?></td>
            <td><?= htmlentities($r['name']) ?></td>
            <td><?= htmlentities($r['email']) ?></td>
            <td><?= htmlentities($r['mobile']) ?></td>
            <td><?= htmlentities($r['birthday']) ?></td>
            <td><?= htmlentities($r['address']) ?></td>
            </tr>
        <?php } ?>
        </tbody>
        </table>
    </div>
</div>
<?php include __DIR__.'/0823_html_foot.php' ?>
