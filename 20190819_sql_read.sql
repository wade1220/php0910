SELECT * FROM `address_book` WHERE `name` = '王曉明'            -- WHERE 後面加的是條件

SELECT * FROM `address_book` WHERE `sid` > 5                    -- 數字可不加單引號 ''

SELECT * FROM `address_book` WHERE `name` LIKE '%王曉明%'       -- 找字串裡的
SELECT * FROM `address_book` WHERE `name` LIKE '王曉明%'        -- 找字首
SELECT * FROM `address_book` WHERE `name` LIKE '%王曉明'        -- 找字尾

SELECT * FROM `address_book` WHERE `name` = '%2%' or `mobile` = '%2%'   --可以用關係運算子，邏輯運算子