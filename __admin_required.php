<?php
//這是一隻沒登入就離開的程式(只要有想要擋掉的頁面，就引入這隻)

if(! isset($_SESSION)){
    session_start();
}
if(! isset($_SESSION['loginUser'])){
    header('Location: index_.php');
    exit;
}