<?php
require __DIR__. '/__connect_db.php';
$page_name = 'data_insert';
$page_title = '新增資料';

?>
<?php include __DIR__. '/__html_head.php' ?>
<?php include __DIR__. '/__navbar.php' ?>

<style>
    small.form-text {
        color: red;
    }
</style>

<div class="container">
<div style="margin-top: 2rem;">
    <div class="row">
        <div class="col">
            <div class="alert alert-primary" role="alert" id="info-bar" style="display: none"></div>    <!-- 顯示成功或失敗欄位訊息 -->
        </div>
    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">新增資料</h5>
                    <form  name="form1" method="post" onsubmit="return checkForm()">                      <!-- 不需要action="data_insert_api.php"，不直接刷新頁面，用AJAX -->
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name">
                            <small id="nameHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="email">電子郵箱</label>
                            <input type="text" class="form-control" id="email" name="email">
                            <small id="emailHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                            <small id="mobileHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" value="2019-10-30">
                            <small id="birthdayHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <input type="text" class="form-control" id="address" name="address">
                            <small id="addressHelp" class="form-text"></small>
                        </div>


                        <button type="submit" class="btn btn-primary">新增</button>
                    </form>
                </div>
            </div>

        </div>
    </div>





</div>
<script>
    //  function checkForm(){                          //AJAX的除錯方法，可勾選preserve log來除錯，因為AJAX其實會刷新，只是看起來不會(console會被刷新)
    //         // TODO: 檢查必填欄位, 欄位值的格式

    //         let fd = new FormData(document.form1);  //先建立一個FormData物件，將值放入

    //         fetch('data_insert_api.php', {          //呼叫這個php，先呼叫後就跳到最後一行return false,讓他自己去執行
    //             method: 'POST',                     //用甚麼方法
    //             body: fd,                           //將資料(fd)放在http的body裡
    //         })
    //             .then(response=>{   
    //                 return response.text();         //轉成文字
    //             })
    //             .then(txt=>{                        //輸出文字提醒
    //                 alert(txt);
    //             });

    //         return false; // 表單不出用傳統的 post 方式送出
    //     }

    //     let info_bar = document.querySelector('#info-bar');
        let info_bar = document.querySelector('#info-bar');
        let name = document.querySelector('#name');
        let mobile = document.querySelector('#mobile');
        let mobilePattern = /^09\d{2}\-?\d{3}\-?\d{3}$/;                      //mobile的欄位檢查條件  //包起來 09表示前面一定是09，\d表示數字，{8}表示幾個數字
        
        //   /[A-Z]{2}\d{8}/i  統一發票
        //   /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i   信箱

        function checkForm(){
            let isPass = true;                  //設一個變數看有沒有過，沒過就不送出表單

            // TODO: 檢查必填欄位, 欄位值的格式
            if(name.value.length<2){
                name.style.border = '1px solid red';
                name.closest('.form-group').querySelector('small').innerText = '請填寫正確的姓名';      
                // return false;
                isPass = false;
            }

            if(! mobilePattern.test(mobile.value)){                 //test()是一個function可以去檢查裡面的值有沒有符合正則表示法
                mobile.style.border = '1px solid red';
                mobile.closest('.form-group').querySelector('small').innerText = '請填寫正確的手機格式';
                isPass = false;
            }



            let fd = new FormData(document.form1);

            if(isPass){                                 //如果驗證有通過(isPass = true)，才送出到後端
                fetch('data_insert_api.php', {
                    method: 'POST',
                    body: fd,
                })
                    .then(response=>{                   //後端用json格式送回來，前端將json格式轉回來就是"物件"
                        return response.json();         //只要在fetch function指定要執行的php程式裡面，用echo 印出任何東西都會回傳到promise(response)物件裡
                    })
                    .then(json=>{
                        console.log(json);
                        info_bar.style.display = 'block';
                        info_bar.innerHTML = json.info;
                        if(json.success){
                            info_bar.className = 'alert alert-success';
                        } else {
                            info_bar.className = 'alert alert-danger';
                        }
                    });
            }
            return false; // 表單不出用傳統的 post 方式送出
        }
                

</script>


</div>
<?php include __DIR__. '/__html_foot.php' ?>

