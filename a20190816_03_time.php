<?php
date_default_timezone_set('Asia/Taipei');   

echo time(). '<br>';

echo date("Y-m-d H:i:s"). '<br>';                  //Y:年  m:月 d:天  H:小時 i:分 s:秒
echo date("Y-m-d", time()+30*24*60*60). '<br>';    //30天後的日期
echo date("Y-m-d", strtotime('+30 days')). '<br>';



// strtotime();  可以將日期文字檔格式轉成時間格式。
// 可去 php.net找